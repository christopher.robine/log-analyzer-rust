use std::collections::HashMap;
use std::fs::{self, File};
use std::io::{self, BufRead};
use regex::Regex;
use std::time::Instant;
use std::fs::metadata;
use indicatif::{ProgressBar, ProgressStyle};

fn main() {
    let start = Instant::now();
    let path = "C:\\Users\\Chris\\Downloads\\ssl_access_log\\ssl_access_log";
    let mut total_requests = 0;
    let mut unique_ips = HashMap::new();
    let mut status_codes = HashMap::new();
    let mut ip_requests = HashMap::new();
    if let Err(e) = process_logs(path, &mut total_requests, &mut unique_ips, &mut status_codes, &mut ip_requests) {
        eprintln!("Erreur lors du traitement des logs: {}", e);
        return;
    }
    
    // Affichage des résultats
    println!("Nombre total de requêtes: {}", total_requests);
    println!("Nombre total d'adresses IP uniques: {}", unique_ips.len());
    println!("Requêtes par code de statut HTTP:");
    for (code, count) in status_codes {
        println!("{}: {}", code, count);
    }
    if let Some((ip, &count)) = ip_requests.iter().max_by_key(|entry| entry.1) {
        let percentage = (count as f64 / total_requests as f64) * 100.0;
        println!("Adresse IP avec le plus de requêtes: {} ({} requêtes, {:.2}%)", ip, count, percentage);
    }
    
    let duration = start.elapsed(); // Calculez la durée de l'exécution
    println!("Temps d'exécution: {:?}", duration); // Affichez le temps d'exécution
}

fn process_logs(path: &str, total_requests: &mut u32, unique_ips: &mut HashMap<String, u32>, status_codes: &mut HashMap<String, u32>, ip_requests: &mut HashMap<String, u32>) -> io::Result<()> {
    let file = File::open(path)?;
    let metadata = fs::metadata(path)?;
    let total_size = metadata.len() as usize;
    let reader = io::BufReader::new(file);
    let re_ip = Regex::new(r"\b(?:\d{1,3}\.){3}\d{1,3}\b").unwrap();
    let re_http_code = Regex::new(r"\b((2[0][0-9]|210)|(3[0-1][0])|(4[0-2][0-9]|430)|(5[0][0-1][0-1]|52[0-7]))\b").unwrap();
    let mut processed_bytes = 0usize; // Octets déjà lus et traités

    // Initialisez la barre de progression
    let pb = ProgressBar::new(total_size as u64);
    pb.set_style(ProgressStyle::default_bar()
        .template("{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {bytes}/{total_bytes} ({eta})")
        .progress_chars("#>-"));

    for line in reader.lines() {
        let line = line?;
        processed_bytes += line.len() + 1; // +1 pour le caractère de fin de ligne

        if let Some(ip_caps) = re_ip.captures(&line) {
            let ip = ip_caps.get(0).unwrap().as_str().to_string();
            *unique_ips.entry(ip.clone()).or_insert(0) += 1;
            *ip_requests.entry(ip).or_insert(0) += 1;
        }
        if let Some(code_caps) = re_http_code.captures(&line) {
            *total_requests += 1;
            let code = code_caps.get(0).unwrap().as_str().to_string();
            *status_codes.entry(code).or_insert(0) += 1;
        }

        pb.inc(line.len() as u64); // Incrémentez la barre de progression en fonction de la longueur de la ligne
    }

    pb.finish_with_message("Traitement complet");
    Ok(())
}